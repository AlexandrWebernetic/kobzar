$(function(){
$('.slider-box__list').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: $('.slider-btn--prev'),
    nextArrow: $('.slider-btn--next'),
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 1.2,
            }
        }
    ]
});
});
//order
$('.btn-order').on('click', function (){
    $('body').addClass('modal-open');
    $('.consultation-popup').addClass('open');
});
$('.popup-close').on('click', function () {
    $('body').removeClass('modal-open');
    $('.consultation-popup').removeClass('open');
});
//popup-slider

$('.slider-for__box').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav__box'
});
$('.slider-nav__box').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: false,
    asNavFor: '.slider-for__box',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    arrows: false
});
//product-popup
$('.vouchers-item ').on('click', function (){
    $('body').addClass('modal-product-open');
    $('.product-popup').addClass('open');
});
$('.popup-close').on('click', function () {
    $('body').removeClass('modal-product-open');
    $('.product-popup').removeClass('open');
});
//mobile-menu
$('.header-burger').on('click', function () {
    $('body').addClass('menu-open');
    $('.header-navigation').addClass('open');
});
$('.menu-close').on('click', function () {
    $('body').removeClass('menu-open');
    $('.header-navigation').removeClass('open');
});