<?php


namespace kobzar;


class Enqueue
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', array($this, 'enqueueScripts'));

        add_action('wp_enqueue_scripts', array($this, 'enqueueStyles'));
    }

    public function enqueueScripts()
    {

        wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js', array('jQuery'), null, true);

        wp_enqueue_script('jQuery', get_template_directory_uri() . '/assets/js/jquery.js', array(), null, true);

        wp_enqueue_script('main.js', get_template_directory_uri() . '/assets/js/main.js', array(), null, true);

    }

    public function enqueueStyles()
    {
        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap-grid.min.css');

        wp_enqueue_style('normalize', get_template_directory_uri() . '/assets/css/normalize.css');

        wp_enqueue_style('slick', get_template_directory_uri() . '/assets/slick.css');

        wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.min.css');

        wp_enqueue_style('custom', get_template_directory_uri() . '/assets/css/custom.css');

    }

}
