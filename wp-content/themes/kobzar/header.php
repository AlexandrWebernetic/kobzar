<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KOBZAR</title>
    <?php wp_head(); ?>
</head>
<body>
<div id="modal">
    <div class="consultation-popup">
        <div class="popup-close">
            <img src="img/close.svg" alt="">
        </div>
        <form action="#" class="consultation-form">
            <div class="consultation-form__box">
                <div class="consultation-form__box-title">Консультация</div>
                <div class="consultation-form__box-text">Введите свои контакты и мы позвоним вам</div>
                <div class="consultation-form__box-list">
                    <div class="consultation-form__list-item">
                        <label>Ваше имя</label>
                        <input type="text" placeholder="Введите ваше имя">
                    </div>
                    <div class="consultation-form__list-item">
                        <label>Телефон</label>
                        <input type="text" placeholder="Введите ваш телефон">
                    </div>
                    <div class="consultation-form__list-item">
                        <label>Почта</label>
                        <input type="text" placeholder="Введите ваш email">
                    </div>
                </div>
                <button class="btn btn-invert" href="#">
                    <span>Заказать тур</span>
                    <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                    </svg>
                </button>
            </div>
        </form>
    </div>
</div>
<div id="modal-product">
    <div class="product-popup">
        <div class="popup-close">
            <img src="img/close.svg" alt="">
        </div>
        <div class="product-popup__box">
            <div class="popup-slider">
                <div class="slider-for">
                    <div class="slider-for__box">
                        <div class="slider-for__box-item">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="slider-for__box-item">
                            <img src="img/vouchers-item-2.png" alt="">
                        </div>
                        <div class="slider-for__box-item">
                            <img src="img/vouchers-item-3.png" alt="">
                        </div>
                        <div class="slider-for__box-item">
                            <img src="img/vouchers-item-4.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="slider-nav">
                    <div class="slider-nav__box">
                        <div class="slider-nav__box-item">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="slider-nav__box-item">
                            <img src="img/vouchers-item-2.png" alt="">
                        </div>
                        <div class="slider-nav__box-item">
                            <img src="img/vouchers-item-3.png" alt="">
                        </div>
                        <div class="slider-nav__box-item">
                            <img src="img/vouchers-item-4.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-info">
                <div class="product-info__box">
                    <div class="product-info__box-price">
                        <div class="title">Туры в Испанию</div>
                        <span>217$</span>
                    </div>
                    <div class="vouchers-item__description-tags">
                        <div class="vouchers-item__tags-item">
                            <img src="img/calendar.svg" alt="">
                            <span>12 дней</span>
                        </div>
                        <div class="vouchers-item__tags-item">
                            <img src="img/beach.svg" alt="">
                            <span>Пляжный курорт</span>
                        </div>
                        <div class="vouchers-item__tags-item">
                            <img src="img/night.svg" alt="">
                            <span>Без ночных переездов</span>
                        </div>
                    </div>
                    <div class="product-info__box-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis amet, placerat nec eget
                        interdum. Aliquet velit quis turpis amet ornare maecenas ac. Dignissim sed cum sem augue massa.
                        Senectus sed nec, phasellus eu. Vitae libero, non ut donec egestas augue. Blandit ut diam
                        tristique ut elit tellus aliquet. Est ut rhoncus dolor, a enim, nisl amet egestas eget. Duis
                        lorem pretium gravida lobortis pellentesque fringilla donec.
                    </div>
                    <div class="product-info__box-nav">
                        <a class="btn btn-invert" href="#">
                            <span>Заказать тур</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                        <a class="btn" href="#">
                            <span>Консультация</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="header">
    <div class="container-fluid">
        <div class="header__box">
            <button class="header-burger">
                <img src="img/header-burger.svg" alt="">
            </button>
            <?php the_custom_logo(); ?>
            <nav class="header-navigation">
                <div class="menu-close">
                    <img src="img/close.svg" alt="">
                </div>
                <?php
                    wp_nav_menu(array(
                            'menu' => 'Меню в шапке сайта',
                            'menu_class' => 'header-navigation__list',
                            'container'       => '',
                            'menu_id'         => 'header-menu',
                    ));
                ?>


                <!--                <ul class="header-navigation__list">-->
                <!--                    <li class="header-navigation__list-item"><a href="#">Туры</a></li>-->
                <!--                    <li class="header-navigation__list-item"><a href="#">Горящие туры</a></li>-->
                <!--                    <li class="header-navigation__list-item"><a href="#">Услуги</a></li>-->
                <!--                    <li class="header-navigation__list-item"><a href="#">О компании</a></li>-->
                <!--                    <li class="header-navigation__list-item"><a href="#">Контакты</a></li>-->
                <!--                </ul>-->
            </nav>
            <a class="btn btn-order" href="#">
                <span>Заказать звонок</span>
                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                </svg>
                <img class="smartphone" src="img/smartphone.svg" alt="">
            </a>
        </div>
    </div>
</header>
