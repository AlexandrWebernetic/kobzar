<?php get_header() ?>
<?php get_template_part( 'template-parts/main', 'block' ); ?>
<section class="slider">
    <div class="container-fluid">
        <div class="title left">
            Лучшие предложения этой недели
        </div>
        <div class="slider-navigation">
            <div class="slider-tabs">
                <div class="slider-tabs__box">
                    <div class="slider-tabs__box-item active">Европа</div>
                    <div class="slider-tabs__box-item">Азия</div>
                    <div class="slider-tabs__box-item">Острова</div>
                    <div class="slider-tabs__box-item">Горы</div>
                    <div class="slider-tabs__box-item">Пляж</div>
                </div>
            </div>
            <div class="slider-btns">
                <div class="slider-btn slider-btn--prev">
                    <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5 1L1.5 5L5.5 9" stroke-width="1.5"/>
                    </svg>

                </div>
                <div class="slider-btn slider-btn--next">
                    <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                    </svg>
                </div>
            </div>
        </div>
        <div class="slider-box">
            <div class="slider-box__list">
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="vouchers-item ">
                    <div class="vouchers-item__box">
                        <div class="vouchers-item__box-images">
                            <img src="img/vouchers-item-1.png" alt="">
                        </div>
                        <div class="vouchers-item__box-description">
                            <div class="vouchers-item__description-info">
                                <div class="vouchers-item__info-title">Испания</div>
                                <div class="vouchers-item__info-price">980 BYN/чел</div>
                            </div>
                            <div class="vouchers-item__description-tags">
                                <div class="vouchers-item__tags-item">
                                    <img src="img/calendar.svg" alt="">
                                    <span>12 дней</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/beach.svg" alt="">
                                    <span>Пляжный курорт</span>
                                </div>
                                <div class="vouchers-item__tags-item">
                                    <img src="img/night.svg" alt="">
                                    <span>Без ночных переездов</span>
                                </div>
                            </div>
                            <a class="btn" href="#">
                                <span>Заказать звонок</span>
                                <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part( 'template-parts/cooperation'); ?>
<section class="vouchers">
    <div class="container-fluid">
        <div class="title">Популярные направления сейчас</div>
        <div class="vouchers__list row">
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-1.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Испания</div>
                            <div class="vouchers-item__info-price">980 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>12 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/beach.svg" alt="">
                                <span>Пляжный курорт</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/night.svg" alt="">
                                <span>Без ночных переездов</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-2.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Испания</div>
                            <div class="vouchers-item__info-price">980 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>12 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/beach.svg" alt="">
                                <span>Пляжный курорт</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/night.svg" alt="">
                                <span>Без ночных переездов</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-3.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Париж</div>
                            <div class="vouchers-item__info-price">250 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>5 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/beach.svg" alt="">
                                <span>Пляжный курорт</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/holiday.svg" alt="">
                                <span>На праздничные дни</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-4.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Лондон</div>
                            <div class="vouchers-item__info-price">719 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>5 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/night.svg" alt="">
                                <span>Без ночных переездов</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-1.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Испания</div>
                            <div class="vouchers-item__info-price">980 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>12 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/beach.svg" alt="">
                                <span>Пляжный курорт</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/night.svg" alt="">
                                <span>Без ночных переездов</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-2.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Испания</div>
                            <div class="vouchers-item__info-price">980 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>12 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/beach.svg" alt="">
                                <span>Пляжный курорт</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/night.svg" alt="">
                                <span>Без ночных переездов</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-3.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Испания</div>
                            <div class="vouchers-item__info-price">980 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>12 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/beach.svg" alt="">
                                <span>Пляжный курорт</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/night.svg" alt="">
                                <span>Без ночных переездов</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vouchers-item col-xl-3 col-lg-4 col-sm-6 col-12">
                <div class="vouchers-item__box">
                    <div class="vouchers-item__box-images">
                        <img src="img/vouchers-item-4.png" alt="">
                    </div>
                    <div class="vouchers-item__box-description">
                        <div class="vouchers-item__description-info">
                            <div class="vouchers-item__info-title">Испания</div>
                            <div class="vouchers-item__info-price">980 BYN/чел</div>
                        </div>
                        <div class="vouchers-item__description-tags">
                            <div class="vouchers-item__tags-item">
                                <img src="img/calendar.svg" alt="">
                                <span>12 дней</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/beach.svg" alt="">
                                <span>Пляжный курорт</span>
                            </div>
                            <div class="vouchers-item__tags-item">
                                <img src="img/night.svg" alt="">
                                <span>Без ночных переездов</span>
                            </div>
                        </div>
                        <a class="btn" href="#">
                            <span>Заказать звонок</span>
                            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-invert" href="#">
            <span>Выбрать направление</span>
            <svg viewBox="0 0 7 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 1L5 5L1 9" stroke-width="1.5"/>
            </svg>
        </a>
    </div>
</section>
<section class="services">
    <div class="container-fluid">
        <div class="title">Почему выбирают Kobzar</div>
        <div class="services__list row">
            <div class="services-item col-xl-3 col-sm-6 col-12">
                <div class="services-item__box">
                    <div class="services-item__box-images">
                        <img src="img/services-item-1.svg" alt="">
                    </div>
                    <div class="services-item__box-text">
                        Туры во все страны мира
                    </div>
                </div>
            </div>
            <div class="services-item col-xl-3 col-sm-6 col-12">
                <div class="services-item__box">
                    <div class="services-item__box-images">
                        <img src="img/services-item-2.svg" alt="">
                    </div>
                    <div class="services-item__box-text">
                        Мы точно знаем, что продаем
                    </div>
                </div>
            </div>
            <div class="services-item col-xl-3 col-sm-6 col-12">
                <div class="services-item__box">
                    <div class="services-item__box-images">
                        <img src="img/services-item-3.svg" alt="">
                    </div>
                    <div class="services-item__box-text">
                        Круглосуточная поддержка
                    </div>
                </div>
            </div>
            <div class="services-item col-xl-3 col-sm-6 col-12">
                <div class="services-item__box">
                    <div class="services-item__box-images">
                        <img src="img/services-item-4.svg" alt="">
                    </div>
                    <div class="services-item__box-text">
                        Актуальные цены
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="reviews">
    <div class="container-fluid">
        <div class="title">Что о нас говорят клиенты</div>
        <div class="reviews__list row">
            <div class="reviews-item col-lg-6 col-12">
                <div class="reviews-item__box">
                    <div class="reviews-item__box-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sagittis tortor adipiscing molestie
                        fames lobortis faucibus orci malesuada non. Tincidunt lacinia at leo id ultrices proin nec in.
                        Sed augue est sit arcu tristique enim ut lacinia. Orci viverra lorem varius ac. Habitasse velit,
                        etiam integer et, mauris. Vulputate nulla volutpat purus imperdiet.
                    </div>
                    <div class="reviews-item__reviewer">
                        <div class="reviews-item__reviewer-images">
                            <img src="img/reviewer.png" alt="">
                        </div>
                        <div class="reviews-item__reviewer-info">
                            <div class="reviews-item__reviewer-name">
                                Ольга Викторовна Шульгина
                            </div>
                            <div class="reviews-item__reviewer-tour">
                                Тур в Мадрид
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="reviews-item col-lg-6 col-12">
                <div class="reviews-item__box">
                    <div class="reviews-item__box-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sagittis tortor adipiscing molestie
                        fames lobortis faucibus orci malesuada non. Tincidunt lacinia at leo id ultrices proin nec in.
                        Sed augue est sit arcu tristique enim ut lacinia. Orci viverra lorem varius ac. Habitasse velit,
                        etiam integer et, mauris. Vulputate nulla volutpat purus imperdiet.
                    </div>
                    <div class="reviews-item__reviewer">
                        <div class="reviews-item__reviewer-images">
                            <img src="img/reviewer.png" alt="">
                        </div>
                        <div class="reviews-item__reviewer-info">
                            <div class="reviews-item__reviewer-name">
                                Ольга Викторовна Шульгина
                            </div>
                            <div class="reviews-item__reviewer-tour">
                                Тур в Мадрид
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="reviews-item col-lg-6 col-12">
                <div class="reviews-item__box">
                    <div class="reviews-item__box-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sagittis tortor adipiscing molestie
                        fames lobortis faucibus orci malesuada non. Tincidunt lacinia at leo id ultrices proin nec in.
                        Sed augue est sit arcu tristique enim ut lacinia. Orci viverra lorem varius ac. Habitasse velit,
                        etiam integer et, mauris. Vulputate nulla volutpat purus imperdiet.
                    </div>
                    <div class="reviews-item__reviewer">
                        <div class="reviews-item__reviewer-images">
                            <img src="img/reviewer.png" alt="">
                        </div>
                        <div class="reviews-item__reviewer-info">
                            <div class="reviews-item__reviewer-name">
                                Ольга Викторовна Шульгина
                            </div>
                            <div class="reviews-item__reviewer-tour">
                                Тур в Мадрид
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="reviews-item col-lg-6 col-12">
                <div class="reviews-item__box">
                    <div class="reviews-item__box-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sagittis tortor adipiscing molestie
                        fames lobortis faucibus orci malesuada non. Tincidunt lacinia at leo id ultrices proin nec in.
                        Sed augue est sit arcu tristique enim ut lacinia. Orci viverra lorem varius ac. Habitasse velit,
                        etiam integer et, mauris. Vulputate nulla volutpat purus imperdiet.
                    </div>
                    <div class="reviews-item__reviewer">
                        <div class="reviews-item__reviewer-images">
                            <img src="img/reviewer.png" alt="">
                        </div>
                        <div class="reviews-item__reviewer-info">
                            <div class="reviews-item__reviewer-name">
                                Ольга Викторовна Шульгина
                            </div>
                            <div class="reviews-item__reviewer-tour">
                                Тур в Мадрид
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>
